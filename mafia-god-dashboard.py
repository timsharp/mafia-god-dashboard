from tkinter import *
from tkinter import messagebox
import random
from collections import Counter

root = Tk()
root.title('Mafia GOD Dashboard © Tim Sharp 2.0')
# root.iconbitmap('public/mafia-icon.ico')
root.geometry('1200x700')


players, roles = [], []
victims, hospitalized_people = [], []
current_players, current_roles, dead_players = [], [], []
players_n_roles = {}

# magician_is_dead = False

global roles_count
roles_count = Counter(current_roles)

global victims_count
victims_count = Counter(victims)

number_of_players, number_of_mafias, number_of_villagers, number_of_doctors, number_of_sheriffs, number_of_magicians, number_of_kamikazes, number_of_maniacs = IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar(), IntVar()
number_of_good_people, number_of_bad_people = IntVar(), IntVar()
person_mafias_kill, person_kamikaze_asks, person_magician_mutes, person_maniac_kills, person_doctor_saves, person_villagers_execute = StringVar(), StringVar(), StringVar(), StringVar(), StringVar(), StringVar()

preferred_text_font = ('Cambria', 10)


# function to return key for any value
def get_key(val, dictionary):
    for key, value in dictionary.items():
         if val == value:
             return key
    return "Key doesn't exist"


def add_roles_to_game():
    roles.clear()
    for m in range(number_of_mafias.get()):
        roles.append('Mafia')

    for k in range(number_of_kamikazes.get()):
        roles.append('Kamikaze')

    for d in range(number_of_doctors.get()):
        roles.append('Doctor')

    for s in range(number_of_sheriffs.get()):
        roles.append('Sheriff')

    for m in range(number_of_magicians.get()):
        roles.append('Magician')

    for m in range(number_of_maniacs.get()):
        roles.append('Maniac')

    for v in range(number_of_villagers.get()):
        roles.append('Villager')

    global current_roles
    current_roles = roles

    number_of_players_entry.config(state=DISABLED)
    number_of_mafias_entry.config(state=DISABLED)
    number_of_villagers_entry.config(state=DISABLED)
    number_of_doctors_entry.config(state=DISABLED)
    number_of_sheriffs_entry.config(state=DISABLED)
    number_of_magicians_entry.config(state=DISABLED)
    number_of_kamikazes_entry.config(state=DISABLED)
    number_of_maniacs_entry.config(state=DISABLED)

    global number_of_effective_players
    number_of_effective_players =  number_of_mafias.get() + number_of_villagers.get() + number_of_doctors.get() +  number_of_sheriffs.get() + number_of_magicians.get() + number_of_kamikazes.get() + number_of_maniacs.get()

    if number_of_effective_players == number_of_players.get() and number_of_effective_players >= 6 and number_of_mafias.get() >= 2 and number_of_villagers.get() >= 2 and number_of_doctors.get() >= 1:
        messagebox.showinfo(title='Success!', message=f'{number_of_mafias.get()} mafias, {number_of_villagers.get()} villagers, {number_of_doctors.get()} doctor(s), {number_of_sheriffs.get()} sheriff(s), {number_of_magicians.get()} magician(s), {number_of_kamikazes.get()} kamikaze(s), and {number_of_maniacs.get()} maniac(s) have been added to the game.\n\n\nIf you would like to proceed to the next step, click `OK` and start adding the names of the players (one name in each line)\n\n\nIf you would like to change the number of players, go back to the previous screen and click the `Reset/Edit` button.')
        random_assignment_btn.config(text=f'Click to assign roles to these (👆) {number_of_players.get()} players randomly. (👇)', state=NORMAL)
        reshuffle_btn.config(state=NORMAL)
        player_names_text.config(state=NORMAL)
        player_names_text.delete('1.0', END)
        player_names_label.config(text=f'Name of players: \n➡➡➡➡➡➡➡\n➡➡➡➡➡➡➡\n➡➡➡➡➡➡➡\nONE PLAYER IN EACH LINE\n\nPut exactly {number_of_effective_players} names.')
    else:
        messagebox.showerror('Error!', f'Please check if your configuration (number of players and roles) is correct. \n\n1) Add at least 7 players for a fun game. \n\n2) Please make sure you add at least 2 (or more) mafias, 2 (or more) villagers, and 1 (or more) doctor. \n\nClick `Reset/Edit` to configure the number of players.')



def random_assignment():
    assigned_roles_text.config(state=NORMAL)
    assigned_roles_text.delete('1.0', END)
    global players
    players = player_names_text.get(1.0, END).split('\n')

    players.pop(-1)

    global current_players
    current_players = players
    
    if number_of_players.get() == len(players):
        random.shuffle(players)

        players_n_roles.clear()
        for i in range(len(players)):
            players_n_roles[players[i]] = roles[i]
        
        assigned_roles_text.config(state=NORMAL)
        for i in range(len(players)):
            assigned_roles_text.insert(END, f'{players[i]} - {players_n_roles[players[i]]}\n')
        
        player_names_text.config(state=DISABLED)
        assigned_roles_text.config(state=DISABLED)
        start_game_btn.config(state=NORMAL)
    else:
        messagebox.showerror('Error!', f'Please check if you have entered any extra line or forgot to add someone\'s name\n\nRemember: It\'s important to add the same number of players on the list. \n\nClick `Reset/Edit` to configure again.')


def reset_or_edit_roles():
    reset_game_response = messagebox.askquestion('Are you sure? ', 'Do you really want to reset the game? ')
    if reset_game_response == 'yes':
        number_of_players_entry.config(state=NORMAL)
        number_of_mafias_entry.config(state=NORMAL)
        number_of_villagers_entry.config(state=NORMAL)
        number_of_doctors_entry.config(state=NORMAL)
        number_of_sheriffs_entry.config(state=NORMAL)
        number_of_magicians_entry.config(state=NORMAL)
        number_of_kamikazes_entry.config(state=NORMAL)
        number_of_maniacs_entry.config(state=NORMAL)

        player_names_text.config(state=NORMAL)
        player_names_text.delete('1.0', END)
        player_names_text.insert(END, f'Please configure the number of players and roles to add the names of players here.')
        player_names_text.config(state=DISABLED)
        random_assignment_btn.config(state=DISABLED)
        reshuffle_btn.config(state=DISABLED)
        add_roles_to_game_btn.config(state=NORMAL)
        start_game_btn.config(state=DISABLED)
        assigned_roles_text.config(state=DISABLED)
        player_names_label.config(text=f'Name of players: \n➡➡➡➡➡➡➡\n➡➡➡➡➡➡➡\n➡➡➡➡➡➡➡\nONE PLAYER IN EACH LINE')
        
        assigned_roles_text.config(state=NORMAL)
        assigned_roles_text.delete('1.0', END)
        assigned_roles_text.insert(END, f'The game has been reset, please configure again to start the game.')
        assigned_roles_text.config(state=DISABLED)

        currently_alive_text.config(state=NORMAL)
        currently_alive_text.delete('1.0', END)
        currently_alive_text.insert(END, f'The game has been reset, please configure again to start the game.')
        currently_alive_text.config(state=DISABLED)

        currently_dead_text.config(state=NORMAL)
        currently_dead_text.delete('1.0', END)
        currently_dead_text.insert(END, f'The game has been reset, please configure again to start the game.')
        currently_dead_text.config(state=DISABLED)

        current_players.clear()
        dead_players.clear()
        current_roles.clear()

        mafias_kill_entry.config(state=DISABLED)
        kamikaze_asks_entry.config(state=DISABLED)
        maniac_kills_entry.config(state=DISABLED)
        doctor_saves_entry.config(state=DISABLED)
        magician_mutes_entry.config(state=DISABLED)
        complete_night_btn.config(state=DISABLED)
        villagers_execute_entry.config(state=DISABLED)
        execute_person_btn.config(state=DISABLED)
        

        roles_count = Counter(current_roles)
        number_of_good_people.set(roles_count['Villager'] + roles_count['Doctor'] + roles_count['Sheriff'])
        number_of_bad_people.set(roles_count['Mafia'])
        
        timer_duration_entry.config(state=DISABLED)
        start_timer_btn.config(state=DISABLED)
        pause_timer_btn.config(state=DISABLED)
        reset_timer_btn.config(state=DISABLED)

        messagebox.showinfo('Success!', 'The game has been reset. You can configure the roles and number of players now. ')
    else:
        pass


def start_game():
    reshuffle_btn.config(state=DISABLED)
    random_assignment_btn.config(state=DISABLED)
    add_roles_to_game_btn.config(state=DISABLED)
    start_game_btn.config(state=DISABLED)
    messagebox.showinfo('Success!', 'The game has started!')

    global number_of_good_people
    global number_of_bad_people
    global roles_count
    roles_count = Counter(current_roles)
    number_of_good_people.set(roles_count['Villager'] + roles_count['Doctor'] + roles_count['Sheriff'])
    number_of_bad_people.set(roles_count['Mafia'])

    currently_alive_text.config(state=NORMAL)
    currently_alive_text.delete('1.0', END)
    global current_players
    global players_n_roles
    for player in current_players:
        currently_alive_text.insert(END, f'{player} - {players_n_roles[player]}\n')
    currently_alive_text.config(state=DISABLED)


    complete_night_btn.config(state=NORMAL)

    mafias_kill_entry.config(state=NORMAL)
    
    if roles_count['Kamikaze'] >= 1:
        kamikaze_asks_entry.config(state=NORMAL)
    
    if roles_count['Maniac'] >= 1:
        maniac_kills_entry.config(state=NORMAL)

    if roles_count['Doctor'] >= 1:
        doctor_saves_entry.config(state=NORMAL)
        
    if roles_count['Magician'] >= 1:
        magician_mutes_entry.config(state=NORMAL)
        
    timer_duration_entry.config(state=NORMAL)
    start_timer_btn.config(state=NORMAL)
    # reset_timer_btn.config(state=NORMAL)



def mute_player():
    global current_players
    global dead_players
    global players_n_roles
    global current_roles
    global roles_count
    the_player = person_magician_mutes.get()
    mute_response = messagebox.askquestion('Are you sure? ', f'Are you sure you want to mute {the_player}?')

    if mute_response == 'yes':
        if the_player in current_players:
            messagebox.showinfo('Muted!', f'{the_player} was muted. ')
            currently_alive_text.config(state=NORMAL)
            currently_alive_text.delete('1.0', END)
            for p in current_players:
                if p == the_player:
                    currently_alive_text.insert(END, f'{p} - {players_n_roles[p]} - (MUTED)\n')
                else: 
                    currently_alive_text.insert(END, f'{p} - {players_n_roles[p]}\n')
            currently_alive_text.config(state=DISABLED)

        else:
            messagebox.showwarning('Warning!', f'{the_player} was not found on the list of alive players. Please enter the correct name. ')
    else:
        messagebox.showinfo('Game Info', f'Okay, {the_player} won\'t be muted. ')

    magician_mutes_entry.delete(0, 'end')



def night_activities():
    confirmation = messagebox.askquestion('Confirmation', 'Are you sure you want to proceed? \n\nPlease check if all the names are correct.')
    if confirmation == 'yes':
        global victims_count
        global victims
        global hospitalized_people
        global current_players
        global dead_players
        global players_n_roles
        global roles_count
        # global magician_is_dead
        

        if len(person_mafias_kill.get()) > 0:
            victims.append(person_mafias_kill.get())
        if len(person_kamikaze_asks.get()) > 0 and players_n_roles[person_kamikaze_asks.get()] == 'Sheriff':
            victims.append(person_kamikaze_asks.get())
        if len(person_maniac_kills.get()) > 0:
            victims.append(person_maniac_kills.get())

        if len(person_doctor_saves.get()) > 0:
            hospitalized_people.append(person_doctor_saves.get())

        if person_doctor_saves.get() in victims:
            victims.remove(person_doctor_saves.get())
            
        
        for the_player in victims:
            kill_response = messagebox.askquestion('Are you sure? ', f'Are you sure you want to kill {the_player}?')
            if kill_response == 'yes':
                if the_player in current_players:
                    # if (players_n_roles[the_player] == 'Magician'):
                    #     magician_is_dead = True
                    if (players_n_roles[the_player] == 'Sheriff' and 'Kamikaze' in current_roles):
                        current_roles.remove('Kamikaze')
                        kamikaze = get_key('Kamikaze', players_n_roles)
                        dead_players.append(kamikaze)
                        current_players.remove(kamikaze)
                        
                        dead_players.append(the_player)
                        current_players.remove(the_player)
                        messagebox.showinfo('Killed!', f'{the_player} was killed. ')
                        
                        
                        currently_alive_text.config(state=NORMAL)
                        currently_alive_text.delete('1.0', END)
                        for p in current_players:
                            currently_alive_text.insert(END, f'{p} - {players_n_roles[p]}\n')
                        currently_alive_text.config(state=DISABLED)
                        
                        currently_dead_text.config(state=NORMAL)
                        currently_dead_text.delete('1.0', END)
                        for p in dead_players:
                            currently_dead_text.insert(END, f'{p} - {players_n_roles[p]}\n')
                        currently_dead_text.config(state=DISABLED)

                        currently_alive_text.config(state=NORMAL)
                        currently_alive_text.delete('1.0', END)
                        for p in current_players:
                            currently_alive_text.insert(END, f'{p} - {players_n_roles[p]}\n')
                        currently_alive_text.config(state=DISABLED)

                        currently_dead_text.config(state=NORMAL)
                        currently_dead_text.delete('1.0', END)
                        for p in dead_players:
                            currently_dead_text.insert(END, f'{p} - {players_n_roles[p]}\n')
                        currently_dead_text.config(state=DISABLED)

                        current_roles.remove(players_n_roles[the_player])

                        global roles_count
                        roles_count = Counter(current_roles)
                        number_of_good_people.set(roles_count['Villager'] + roles_count['Doctor'] + roles_count['Sheriff'])
                        number_of_bad_people.set(roles_count['Mafia'])
                        
                    else:
                        dead_players.append(the_player)
                        current_players.remove(the_player)
                        messagebox.showinfo('Killed!', f'{the_player} was killed. ')
                        
                        currently_alive_text.config(state=NORMAL)
                        currently_alive_text.delete('1.0', END)
                        for p in current_players:
                            currently_alive_text.insert(END, f'{p} - {players_n_roles[p]}\n')
                        currently_alive_text.config(state=DISABLED)

                        currently_dead_text.config(state=NORMAL)
                        currently_dead_text.delete('1.0', END)
                        for p in dead_players:
                            currently_dead_text.insert(END, f'{p} - {players_n_roles[p]}\n')
                        currently_dead_text.config(state=DISABLED)

                        currently_alive_text.config(state=NORMAL)
                        currently_alive_text.delete('1.0', END)
                        for p in current_players:
                            currently_alive_text.insert(END, f'{p} - {players_n_roles[p]}\n')
                        currently_alive_text.config(state=DISABLED)

                        currently_dead_text.config(state=NORMAL)
                        currently_dead_text.delete('1.0', END)
                        for p in dead_players:
                            currently_dead_text.insert(END, f'{p} - {players_n_roles[p]}\n')
                        currently_dead_text.config(state=DISABLED)

                        current_roles.remove(players_n_roles[the_player])
                        # global roles_count
                        roles_count = Counter(current_roles)
                        number_of_good_people.set(roles_count['Villager'] + roles_count['Doctor'] + roles_count['Sheriff'])
                        number_of_bad_people.set(roles_count['Mafia'])
                        
                else:
                    messagebox.showwarning('Warning!', f'{the_player} was not found on the list of alive players. Please enter the correct name. ')
            else:
                messagebox.showinfo('Game Info', f'Okay, {the_player} won\'t be killed. ')


        # magician = get_key('Magician', players_n_roles)
        # if 'Magician' in current_roles or magician_is_dead == False:
        mute_player()
            # magician_is_dead = False


        mafias_kill_entry.delete(0, 'end')
        mafias_kill_entry.config(state=DISABLED)
        kamikaze_asks_entry.delete(0, 'end')
        kamikaze_asks_entry.config(state=DISABLED)
        maniac_kills_entry.delete(0, 'end')
        maniac_kills_entry.config(state=DISABLED)
        doctor_saves_entry.delete(0, 'end')
        doctor_saves_entry.config(state=DISABLED)
        magician_mutes_entry.delete(0, 'end')
        magician_mutes_entry.config(state=DISABLED)

        villagers_execute_entry.config(state=NORMAL)
        execute_person_btn.config(state=NORMAL)
        complete_night_btn.config(state=DISABLED)

        victims.clear()
        hospitalized_people.clear()

    else:
        pass


def execute_player():
    global current_players
    global dead_players
    global players_n_roles
    global current_roles
    global roles_count
    # global magician_is_dead
    
    the_player = person_villagers_execute.get()
    execute_response = messagebox.askquestion('Are you sure? ', f'Are you sure you want to execute {the_player}?')

    if execute_response == 'yes':
        if the_player in current_players:
            
            # if (players_n_roles[the_player] == 'Magician'):
            #     magician_is_dead = False

            dead_players.append(the_player)
            current_players.remove(the_player)
            messagebox.showinfo('Executed!', f'{the_player} was executed. ')
            if (players_n_roles[the_player] == 'Sheriff' and 'Kamikaze' in roles):
                kamikaze = get_key('Kamikaze', players_n_roles)
                kill_kami_response = messagebox.askquestion(f'Execute {kamikaze} too (The Kamikaze)? ', 'Do you want to execute the {kamikaze} along with the Sheriff? ')
                if kill_kami_response == 'yes':
                    current_roles.remove('Kamikaze')
                    dead_players.append(kamikaze)
                    current_players.remove(kamikaze)

                    currently_alive_text.config(state=NORMAL)
                    currently_alive_text.delete('1.0', END)
                    for p in current_players:
                        currently_alive_text.insert(END, f'{p} - {players_n_roles[p]}\n')
                    currently_alive_text.config(state=DISABLED)

                    currently_dead_text.config(state=NORMAL)
                    currently_dead_text.delete('1.0', END)
                    for p in dead_players:
                        currently_dead_text.insert(END, f'{p} - {players_n_roles[p]}\n')
                    currently_dead_text.config(state=DISABLED)
                else:
                    pass

            currently_alive_text.config(state=NORMAL)
            currently_alive_text.delete('1.0', END)
            for p in current_players:
                currently_alive_text.insert(END, f'{p} - {players_n_roles[p]}\n')
            currently_alive_text.config(state=DISABLED)

            currently_dead_text.config(state=NORMAL)
            currently_dead_text.delete('1.0', END)
            for p in dead_players:
                currently_dead_text.insert(END, f'{p} - {players_n_roles[p]}\n')
            currently_dead_text.config(state=DISABLED)

            current_roles.remove(players_n_roles[the_player])

            global roles_count
            roles_count = Counter(current_roles)
            number_of_good_people.set(roles_count['Villager'] + roles_count['Doctor'] + roles_count['Sheriff'])
            number_of_bad_people.set(roles_count['Mafia'])

        else:
            messagebox.showwarning('Warning!', 'The player was not found on the list of alive players. Please enter the correct name. ')
    else:
        pass
        messagebox.showinfo('Game Info', f'Okay, {the_player} won\'t be executed. ')


    villagers_execute_entry.delete(0, 'end')


    complete_night_btn.config(state=NORMAL)

    if roles_count['Mafia'] >= 1:
        mafias_kill_entry.config(state=NORMAL)
    
    if roles_count['Kamikaze'] >= 1:
        kamikaze_asks_entry.config(state=NORMAL)
    
    if roles_count['Maniac'] >= 1:
        maniac_kills_entry.config(state=NORMAL)

    if roles_count['Doctor'] >= 1:
        doctor_saves_entry.config(state=NORMAL)
    
    magician_mutes_entry.config(state=NORMAL)
        
        
    execute_person_btn.config(state=DISABLED)
    villagers_execute_entry.config(state=DISABLED)


timer_is_paused = False
def start_timer():
    global timer_is_paused
    if int(timer_duration.get()) > 0:            
        timer_response = messagebox.askquestion('Confirmation', 'Do you want to start the timer now? ')
        if timer_response == 'yes':
            timer_is_paused = False
            timer_duration_entry.config(state=DISABLED)
            start_timer_btn.config(state=DISABLED)
            pause_timer_btn.config(state=NORMAL)

            duration = timer_duration.get()*60
            global current
            current = duration
            mins, secs = divmod(duration, 60) 
            timer = '{:02d}:{:02d}'.format(mins, secs)
            timer_display_label.config(text=timer)
            
            reset_timer_btn.config(state=NORMAL)
            update_timer()
            
        else:
            pass
    else:
        messagebox.showerror('Error!', 'You gave a wrong input as the duration of the timer. Please put a valid number (greater than zero).')


def update_timer():
    global current
    global timer_is_paused
    if timer_is_paused == False:
        mins, secs = divmod(current, 60)
        current -= 1
        timer = '{:02d}:{:02d}'.format(mins, secs)
        timer_display_label.config(text=timer)
        if current >= 0:
            timer_display_label.after(1000, update_timer)
        else:
            messagebox.showinfo('Time\'s up!', 'The timer has been reset.')
            pause_timer_btn.config(state=DISABLED)
            start_timer_btn.config(state=NORMAL)
            timer_duration_entry.config(state=NORMAL)
            reset_timer_btn.config(state=DISABLED)

def pause_timer():
    global timer_is_paused
    timer_is_paused = not timer_is_paused
    if timer_is_paused == True:
        pause_timer_btn.config(text='Resume Timer')
        timer_display_label.config(fg='red', bg='white')
    else:
        pause_timer_btn.config(text='Pause Timer')
        timer_display_label.config(fg='green', bg='black')
    update_timer()
    

def reset_timer():
    global timer_is_paused
    global reset_timer_response
    reset_timer_response = messagebox.askquestion('Confirm', 'Do you want to reset the timer? ')
    if reset_timer_response == 'yes':
        global current
        current = 0
        timer_display_label.config(text='00:00', fg='green', bg='black')
        
        messagebox.showinfo('Success!', 'The timer has been reset.')
        

        timer_duration_entry.config(state=NORMAL)
        start_timer_btn.config(state=NORMAL)
        pause_timer_btn.config(state=DISABLED)
        timer_is_paused = False
    else:
        pass


game_setup_frame = Frame(root, bg='#61fffa', highlightbackground="#999999", highlightthickness=1)
game_setup_frame.grid(row=0, column=0, pady=20, padx=5)

number_of_players_label = Label(game_setup_frame, text='Number of players: ', font=preferred_text_font)
number_of_players_label.grid(row=0, column=0)
number_of_players_entry = Entry(game_setup_frame, textvariable=number_of_players, font=preferred_text_font)
number_of_players_entry.grid(row=0, column=1)

number_of_mafias_label = Label(game_setup_frame, text='Number of mafias: ', font=preferred_text_font)
number_of_mafias_label.grid(row=1, column=0)
number_of_mafias_entry = Entry(game_setup_frame, textvariable=number_of_mafias, font=preferred_text_font)
number_of_mafias_entry.grid(row=1, column=1)

number_of_villagers_label = Label(game_setup_frame, text='Number of villagers: ', font=preferred_text_font)
number_of_villagers_label.grid(row=2, column=0)
number_of_villagers_entry = Entry(game_setup_frame, textvariable=number_of_villagers, font=preferred_text_font)
number_of_villagers_entry.grid(row=2, column=1)

number_of_doctors_label = Label(game_setup_frame, text='Number of doctor(s): ', font=preferred_text_font)
number_of_doctors_label.grid(row=3, column=0)
number_of_doctors_entry = Entry(game_setup_frame, textvariable=number_of_doctors, font=preferred_text_font)
number_of_doctors_entry.grid(row=3, column=1)

number_of_sheriffs_label = Label(game_setup_frame, text='Number of sheriff(s): ', font=preferred_text_font)
number_of_sheriffs_label.grid(row=4, column=0)
number_of_sheriffs_entry = Entry(game_setup_frame, textvariable=number_of_sheriffs, font=preferred_text_font)
number_of_sheriffs_entry.grid(row=4, column=1)

number_of_magicians_label = Label(game_setup_frame, text='Number of magician(s): ', font=preferred_text_font)
number_of_magicians_label.grid(row=5, column=0)
number_of_magicians_entry = Entry(game_setup_frame, textvariable=number_of_magicians, font=preferred_text_font)
number_of_magicians_entry.grid(row=5, column=1)

number_of_kamikazes_label = Label(game_setup_frame, text='Number of kamikaze(s): ', font=preferred_text_font)
number_of_kamikazes_label.grid(row=6, column=0)
number_of_kamikazes_entry = Entry(game_setup_frame, textvariable=number_of_kamikazes, font=preferred_text_font)
number_of_kamikazes_entry.grid(row=6, column=1)

number_of_maniacs_label = Label(game_setup_frame, text='Number of maniac(s): ', font=preferred_text_font)
number_of_maniacs_label.grid(row=7, column=0)
number_of_maniacs_entry = Entry(game_setup_frame, textvariable=number_of_maniacs, font=preferred_text_font)
number_of_maniacs_entry.grid(row=7, column=1)

add_roles_to_game_btn = Button(game_setup_frame, text='Add these Roles to the Game', command=add_roles_to_game, font=preferred_text_font, fg='green', relief=RAISED)
add_roles_to_game_btn.grid(row=8, column=1, pady=10, padx=20)
reset_or_edit_roles_btn = Button(game_setup_frame, text='Reset/Edit', command=reset_or_edit_roles, font=preferred_text_font, fg='orange', relief=RAISED)
reset_or_edit_roles_btn.grid(row=8, column=2, pady=10, sticky='E')

player_names_label = Label(game_setup_frame, text=f'Name of players: \n➡➡➡➡➡➡➡\n➡➡➡➡➡➡➡\n➡➡➡➡➡➡➡\nONE PLAYER IN EACH LINE', font=preferred_text_font)
player_names_label.grid(row=9, column=0)
player_names_text = Text(game_setup_frame, height=12, width=25, font=preferred_text_font)
player_names_text.grid(row=9, column=1, pady=10)
player_names_text.insert(END, f'Please configure the number of players and roles to add the names of players here.')
player_names_text.config(state=DISABLED)

random_assignment_btn = Button(game_setup_frame, text=f'Assign Roles Randomly', state=DISABLED, command=random_assignment, font=preferred_text_font, fg='green', relief=RAISED)
random_assignment_btn.grid(row=10, column=0, columnspan=2, pady=10)
reshuffle_btn = Button(game_setup_frame, text='Reshuffle', state=DISABLED, command=random_assignment, font=preferred_text_font, fg='green', relief=RAISED)
reshuffle_btn.grid(row=10, column=2, pady=10)


assigned_roles_label = Label(game_setup_frame, text='The assigned roles are: \n➡➡➡➡➡➡➡\n➡➡➡➡➡➡➡\n➡➡➡➡➡➡➡\nYou can reshuffle if you want', font=preferred_text_font)
assigned_roles_label.grid(row=11, column=0)
assigned_roles_text = Text(game_setup_frame, height=12, width=25, font=preferred_text_font, state=DISABLED)
assigned_roles_text.grid(row=11, column=1, pady=10)
start_game_btn = Button(game_setup_frame, text='Start Game', state=DISABLED, command=start_game, font=preferred_text_font, fg='green', relief=RAISED)
start_game_btn.grid(row=11, column=2, pady=10)


game_status_frame = Frame(root, bg='#61fffa', highlightbackground="#999999", highlightthickness=1)
game_status_frame.grid(row=0, column=1, pady=20, padx=5)

timer_frame = Frame(game_status_frame, bg='#61fffa')
timer_frame.grid(row=0, column=0, columnspan=2, pady=20)

timer_display_label = Label(timer_frame, text='00:00', font=('Helvetica', 48), fg='green', bg='black')
timer_display_label.grid(row=0, column=0, columnspan=3, pady=10)

timer_duration = IntVar()
timer_duration_entry = Entry(timer_frame, textvariable=timer_duration, state=DISABLED)
timer_duration_entry.grid(row=1, column=0, pady=10)

timer_duration_label = Label(timer_frame, text='minutes', font=preferred_text_font)
timer_duration_label.grid(row=1, column=1, pady=10)

start_timer_btn = Button(timer_frame, text='Start Timer', command=start_timer, relief=RAISED, font=preferred_text_font, state=DISABLED)
start_timer_btn.grid(row=2, column=0, pady=10)

pause_timer_btn = Button(timer_frame, text='Pause Timer', command=pause_timer, relief=RAISED, font=preferred_text_font, state=DISABLED)
pause_timer_btn.grid(row=2, column=1, pady=10, padx=10)

reset_timer_btn = Button(timer_frame, text='Reset Timer', command=reset_timer, relief=RAISED, font=preferred_text_font, state=DISABLED)
reset_timer_btn.grid(row=2, column=2, pady=10, padx=10)



good_people_label = Label(game_status_frame, text='Number of good people: ', font=preferred_text_font)
good_people_label.grid(row=1, column=0)

bad_people_label = Label(game_status_frame, text='Number of bad people: ', font=preferred_text_font)
bad_people_label.grid(row=2, column=0)

number_of_good_people_label = Label(game_status_frame, textvariable=number_of_good_people, font=preferred_text_font)
number_of_good_people_label.grid(row=1, column=1, pady=10)

number_of_bad_people_label = Label(game_status_frame, textvariable=number_of_bad_people, font=preferred_text_font)
number_of_bad_people_label.grid(row=2, column=1, pady=10)

currently_alive_label = Label(game_status_frame, text='Currently Alive Players: ', font=preferred_text_font, fg='green')
currently_alive_label.grid(row=3, column=0, pady=10)
currently_alive_text = Text(game_status_frame, height=12, width=25, font=preferred_text_font, state=DISABLED)
currently_alive_text.grid(row=3, column=1, pady=10)

currently_dead_label = Label(game_status_frame, text='Dead Players: \n\n\n\nThey may rest in peace', font=preferred_text_font)
currently_dead_label.grid(row=4, column=0, pady=10, padx=10)
currently_dead_text = Text(game_status_frame, height=8, width=25, font=preferred_text_font, state=DISABLED)
currently_dead_text.grid(row=4, column=1, pady=10, padx=10)


game_control_frame = Frame(root, bg='#61fffa', highlightbackground="#999999", highlightthickness=1)
game_control_frame.grid(row=0, column=2, padx=5, pady=20)


mafias_kill_label = Label(game_control_frame, text='Mafias kill (👉)', font=preferred_text_font, fg='red')
mafias_kill_label.grid(row=0, column=0, pady=10)

mafias_kill_entry = Entry(game_control_frame, fg='red', font=preferred_text_font, state=DISABLED, textvariable=person_mafias_kill)
mafias_kill_entry.grid(row=0, column=1, padx=20, pady=10)

kamikaze_asks_label = Label(game_control_frame, text='Kamikaze asks (👉)', font=preferred_text_font, fg='red')
kamikaze_asks_label.grid(row=1, column=0, pady=10)

kamikaze_asks_entry = Entry(game_control_frame, fg='red', font=preferred_text_font, state=DISABLED, textvariable=person_kamikaze_asks)
kamikaze_asks_entry.grid(row=1, column=1, padx=20, pady=10)

maniac_kills_label = Label(game_control_frame, text='Maniac kills (👉)', font=preferred_text_font, fg='red')
maniac_kills_label.grid(row=2, column=0, pady=10)

maniac_kills_entry = Entry(game_control_frame, fg='red', font=preferred_text_font, state=DISABLED, textvariable=person_maniac_kills)
maniac_kills_entry.grid(row=2, column=1, padx=20, pady=10)

doctor_saves_label = Label(game_control_frame, text='Doctor saves (👉) ', font=preferred_text_font, fg='green')
doctor_saves_label.grid(row=3, column=0, pady=10)

doctor_saves_entry = Entry(game_control_frame, fg='green', font=preferred_text_font, state=DISABLED, textvariable=person_doctor_saves)
doctor_saves_entry.grid(row=3, column=1, padx=20, pady=10)

magician_mutes_label = Label(game_control_frame, text='Magician mutes (👉) ', font=preferred_text_font)
magician_mutes_label.grid(row=4, column=0, pady=10)

magician_mutes_entry = Entry(game_control_frame, font=preferred_text_font, state=DISABLED, textvariable=person_magician_mutes)
magician_mutes_entry.grid(row=4, column=1, padx=20, pady=10)

complete_night_btn = Button(game_control_frame, text='Complete the Night', font=preferred_text_font, state=DISABLED, command=night_activities)
complete_night_btn.grid(row=5, column=0, columnspan=2, padx=20, pady=20)

villagers_execute_label = Label(game_control_frame, text='Villagers execute (👉) ', font=preferred_text_font, fg='red')
villagers_execute_label.grid(row=6, column=0, padx=20)

villagers_execute_entry = Entry(game_control_frame, fg='red', font=preferred_text_font, state=DISABLED, textvariable=person_villagers_execute)
villagers_execute_entry.grid(row=6, column=1, pady=10, padx=20)

execute_person_btn = Button(game_control_frame, text='Execute This Person (👆) ', font=preferred_text_font, state=DISABLED, command=execute_player)
execute_person_btn.grid(row=7, column=0, columnspan=2, pady=20, padx=10)


root.mainloop()